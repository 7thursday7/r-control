import {Controller, Inject} from "@nestjs/common";
import {Gateway} from "./gateway/gateway";
import {MessagePattern, Payload} from "@nestjs/microservices";

@Controller()
export class AppController {
  constructor(
    @Inject(Gateway) private gateway: Gateway
  ) {}

  @MessagePattern({ cmd: 'newVehicle' })
  async newVehicle(@Payload() payload: any) {
    return this.gateway.newVehicle(payload);
  }
}