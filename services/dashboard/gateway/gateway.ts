import {WebSocketGateway, WebSocketServer} from "@nestjs/websockets";
import {Server} from "socket.io";

@WebSocketGateway(9003)
export class Gateway {
  @WebSocketServer()
  server: Server;

  async newVehicle(payload: any) {
    this.server.emit('onCreate', { msg: 'New Vehicle Created', content: payload });
  }
}