import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import {AppController} from "./app.controller";
import {Gateway} from "./gateway/gateway";

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [AppController],
  providers: [Gateway, ConfigService]
})
export class AppModule {}