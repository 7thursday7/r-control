import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseFilters,
  Req,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { VehicleService } from './vehicle.service';
import { CreateVehicleDto } from './dto/create-vehicle.dto';
import { UpdateVehicleDto } from './dto/update-vehicle.dto';
import { TypeORMFilter } from 'services/api/filter/typeorm.filter';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('vehicle')
export class VehicleController {
  constructor(private readonly vehicleService: VehicleService) {}

  @Post()
  @UseFilters(new TypeORMFilter())
  create(@Req() req, @Body() createVehicleDto: CreateVehicleDto) {
    return this.vehicleService.create(req.user.id, createVehicleDto);
  }

  @Post('photo/:id')
  @UseFilters(new TypeORMFilter())
  @UseInterceptors(FileInterceptor('file'))
  addPhoto(@Param('id') id: string, @UploadedFile() file: Express.Multer.File) {
    return this.vehicleService.addPhoto(id, file.buffer, file.originalname);
  }

  @Get()
  findAll() {
    return this.vehicleService.findAll();
  }

  @Get(':id')
  @UseFilters(new TypeORMFilter())
  findOne(@Param('id') id: string) {
    return this.vehicleService.findOne(id);
  }

  @Get('user/:id')
  findUsersVehicles(@Param('id') id) {
    return this.vehicleService.findUserVehicles(id);
  }

  @Patch(':id')
  @UseFilters(new TypeORMFilter())
  update(@Param('id') id: string, @Body() updateVehicleDto: UpdateVehicleDto) {
    return this.vehicleService.update(id, updateVehicleDto);
  }

  @Delete(':id')
  @UseFilters(new TypeORMFilter())
  remove(@Param('id') id: string) {
    return this.vehicleService.remove(id);
  }
}
