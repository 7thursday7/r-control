import {Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CreateVehicleDto } from './dto/create-vehicle.dto';
import { UpdateVehicleDto } from './dto/update-vehicle.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Vehicle } from './entities/vehicle.entity';
import { Repository } from 'typeorm';
import { UserService } from 'services/api/user/user.service';
import { PublicFileService } from 'services/api/public-file/public-file.service';
import {ClientProxy} from "@nestjs/microservices";

@Injectable()
export class VehicleService {
  constructor(
    @InjectRepository(Vehicle)
    private vehicleRepository: Repository<Vehicle>,
    @Inject('DASHBOARD_SERVICE') private dashboardService: ClientProxy,
    private filesService: PublicFileService,
    private userService: UserService,
  ) {}

  async create(id: string, createVehicleDto: CreateVehicleDto) {
    const vehicle = await this.vehicleRepository.create(createVehicleDto);
    const user = await this.userService.findOneById(id);
    vehicle.user = user;
    await this.vehicleRepository.save(vehicle);
    vehicle.user.password = null;

    await this.dashboardService.send({ cmd: 'newVehicle' }, `${vehicle.nmtool} was created by ${user.email}`);

    return vehicle;
  }

  async findAll() {
    return this.vehicleRepository.find();
  }

  async findOne(id: string, relations: string[] = []) {
    const vehicle = this.vehicleRepository.findOne({ where: { id }, relations });

    if (!vehicle)
      throw new NotFoundException('Vehicle with given id was not found');

    return vehicle;
  }

  async findUserVehicles(id: string) {
    const user = await this.userService.findOneById(id, false, ['vehicles']);
    return user.vehicles;
  }

  async addPhoto(vehicleId: string, imageBuffer: Buffer, filename: string) {
    const photo = await this.filesService.uploadPublicFile(imageBuffer, filename);
    const updatedVehicle = this.update(vehicleId, { photo });
    return updatedVehicle;
  }

  async update(id: string, updateVehicleDto: UpdateVehicleDto) {
    this.vehicleRepository.update(id, updateVehicleDto);
    const vehicle = await this.findOne(id);
    return vehicle;
  }

  async remove(id: string) {
    const vehicle = await this.findOne(id);
    const deletedVehicle = await this.vehicleRepository.remove(vehicle);
    return deletedVehicle;
  }
}
