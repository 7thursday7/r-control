import { Module } from '@nestjs/common';
import { VehicleService } from './vehicle.service';
import { VehicleController } from './vehicle.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Vehicle } from './entities/vehicle.entity';
import { UserModule } from 'services/api/user/user.module';
import { PublicFileModule } from 'services/api/public-file/public-file.module';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import {ConfigModule, ConfigService} from '@nestjs/config';

@Module({
  imports: [TypeOrmModule.forFeature([Vehicle]), UserModule, PublicFileModule, ConfigModule.forRoot()],
  controllers: [VehicleController],
  providers: [
    ConfigService,
    VehicleService,
    {
      provide: 'DASHBOARD_SERVICE',
      useFactory: async (configService) => {
        const host = configService.get('RABBITMQ_HOST');
        const queueName = configService.get('RABBITMQ_QUEUE_NAME');

        return ClientProxyFactory.create({
          transport: Transport.RMQ,
          options: {
            urls: [`amqp://${host}`],
            queue: queueName,
            queueOptions: {
              durable: true
            }
          }
        });
      },
      inject: [ConfigService],
    },
  ],
  exports: [VehicleService]
})
export class VehicleModule {}
