import { Action } from '../../action/entities/action.entity';
import { PublicFile } from '../../public-file/entities/public-file.entity';
import { User } from '../../user/entities/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, JoinColumn, OneToMany, OneToOne } from 'typeorm';

@Entity()
export class Vehicle {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false })
  nmtool: string;

  @JoinColumn()
  @ManyToOne(type => User, user => user.vehicles, { cascade: true })
  user: User;

  @JoinColumn()
  @OneToOne(type => PublicFile, { eager: true, nullable: true })
  photo?: PublicFile;

  @JoinColumn()
  @OneToMany(type => Action, action => action.vehicle)
  actions: Action[];
}
