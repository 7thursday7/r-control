import { PartialType } from '@nestjs/mapped-types';
import { CreateVehicleDto } from './create-vehicle.dto';
import { PublicFile } from 'services/api/public-file/entities/public-file.entity';

export class UpdateVehicleDto extends PartialType(CreateVehicleDto) {
  photo?: PublicFile;
}
