import { IsDefined, MaxLength, MinLength } from 'class-validator';

export class CreateVehicleDto {
  @MinLength(5)
  @MaxLength(60)
  @IsDefined()
  nmtool: string;
}
