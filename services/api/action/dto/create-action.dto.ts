import { IsDefined, MaxLength, MinLength } from "class-validator";


export class CreateActionDto {
	@MinLength(5)
	@MaxLength(60)
	@IsDefined()
	description: string;

	@MinLength(5)
	@MaxLength(60)
	@IsDefined()
	vehicle_id: string;
}
