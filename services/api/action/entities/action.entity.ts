import { Task } from '../../task/entities/task.entity';
import { Vehicle } from '../../vehicle/entities/vehicle.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Action {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@JoinColumn()
  @ManyToOne(type => Vehicle, vehicle => vehicle.actions, { cascade: true })
  vehicle: Vehicle;

	@JoinColumn()
  @OneToMany(type => Task, task => task.action)
  tasks: Task[];

	@Column({nullable: false})
	description: string;
}
	
