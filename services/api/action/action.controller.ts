import { Controller, Get, Post, Body, Patch, Param, Delete, UseFilters } from '@nestjs/common';
import { ActionService } from './action.service';
import { CreateActionDto } from './dto/create-action.dto';
import { UpdateActionDto } from './dto/update-action.dto';
import { TypeORMFilter } from 'services/api/filter/typeorm.filter';

@Controller('action')
export class ActionController {
  constructor(private readonly actionService: ActionService) {}

  @Post()
	@UseFilters(new TypeORMFilter())
  create(@Body() createActionDto: CreateActionDto) {
    return this.actionService.create(createActionDto);
  }

  @Get()
  findAll() {
    return this.actionService.findAll();
  }

  @Get(':id')
	@UseFilters(new TypeORMFilter())
  findOne(@Param('id') id: string) {
    return this.actionService.findOne(id);
  }

  @Get('vehicle/:id')
	@UseFilters(new TypeORMFilter())
  findVehiclesActions(@Param('id') id: string) {
    return this.actionService.findVehicleActions(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateActionDto: UpdateActionDto) {
    return this.actionService.update(id, updateActionDto);
  }

  @Delete(':id')
	@UseFilters(new TypeORMFilter())
  remove(@Param('id') id: string) {
    return this.actionService.remove(id);
  }
}
