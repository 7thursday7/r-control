import { Module } from '@nestjs/common';
import { ActionService } from './action.service';
import { ActionController } from './action.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Action } from './entities/action.entity';
import { VehicleModule } from 'services/api/vehicle/vehicle.module';

@Module({
	imports: [TypeOrmModule.forFeature([Action]), VehicleModule],
  controllers: [ActionController],
  providers: [ActionService],
  exports: [ActionService]
})
export class ActionModule {}
