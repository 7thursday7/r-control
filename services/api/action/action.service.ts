import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateActionDto } from './dto/create-action.dto';
import { UpdateActionDto } from './dto/update-action.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Action } from './entities/action.entity';
import { Repository } from 'typeorm';
import { VehicleService } from 'services/api/vehicle/vehicle.service';

@Injectable()
export class ActionService {
	constructor(
		@InjectRepository(Action)
		private actionRepository: Repository<Action>,
    private vehicleService: VehicleService,
	) {}

  async create(createActionDto: CreateActionDto) {
    const action = await this.actionRepository.create(createActionDto);
    const vehicle = await this.vehicleService.findOne(createActionDto.vehicle_id);
    action.vehicle = vehicle;
		
    this.actionRepository.save(action);
		return action;
  }

  async findAll() {
    return this.actionRepository.find();
  }

  async findOne(id: string, relations: string[] = []) {
    const action = this.actionRepository.findOne({ where: { id }, relations });

		if(!action)
		throw new NotFoundException('Action with given id was not found');

		return action;
  }

  async findVehicleActions(id: string) {
    const vehicle = await this.vehicleService.findOne(id, ['actions']);
    return vehicle.actions;
  }

  async update(id: string, updateActionDto: UpdateActionDto) {
    this.actionRepository.update(id, updateActionDto);
		const action = await this.findOne(id);
		return action;
  }

  async remove(id: string) {
    const action = await this.findOne(id);
		const deletedAction = await this.actionRepository.remove(action);
		return deletedAction;
  }
}
