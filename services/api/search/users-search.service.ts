import {Injectable} from "@nestjs/common";
import {ElasticsearchService} from "@nestjs/elasticsearch";
import {User} from "../user/entities/user.entity";
import UserSearchBody from "./types/user-search-body.interface";

@Injectable()
export default class UsersSearchService {
  index = 'users';

  constructor(
    private readonly elasticsearchService: ElasticsearchService,
  ) {}

  async indexUser(user: User) {
    return this.elasticsearchService.index<UserSearchBody>({
      index: this.index,
      document: {
        id: user.id,
        name: user.name,
        email: user.email
      }
    });
  }

  async searchUsers(text: string) {
    const result = await this.elasticsearchService.search<UserSearchBody>({
      index: this.index,
      query: {
        multi_match: {
          query: text,
          fields: ['name', 'email'],
          fuzziness: 'AUTO'
        }
      }
    });

    const hits = result.hits.hits;
    return hits.map((item) => item._source);
  }

  async removeUser(userId: string) {
    this.elasticsearchService.deleteByQuery({
      index: this.index,
      query: {
        match: {
          id: userId
        }
      }
    });
  }

  async updateUser(user: User) {
    const newBody: UserSearchBody = {
      id: user.id,
      name: user.name,
      email: user.email
    };

    const script = Object.entries(newBody).reduce((result, [key, value]) => {
      return `${result} ctx._source.${key}='${value}';`;
    }, '');

    return this.elasticsearchService.updateByQuery({
      index: this.index,
      query: {
        match: {
          id: user.id,
        }
      },
      script: {
        source: script
      }
    });
  }
}