export default interface UserSearchBody {
  id: string;
  name: string;
  email: string;
}