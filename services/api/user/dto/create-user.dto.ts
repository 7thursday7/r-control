import { IsDefined, IsEmail, MaxLength, MinLength } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  @IsDefined()
  email: string;

  @IsDefined()
  @MinLength(4)
  @MaxLength(32)
  name: string;

  @MinLength(8)
  @MaxLength(64)
  @IsDefined()
  password: string;
}
