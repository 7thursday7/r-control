import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Role } from '../enums/Role';
import { Vehicle } from '../../vehicle/entities/vehicle.entity';
import { Exclude } from 'class-transformer';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'enum', enum: Role, array: true, nullable: false, default: [Role.Customer] })
  roles: Role[];

  @Column({ type: 'varchar', nullable: false })
  name: string;

  @Column({ nullable: false, unique: true })
  email: string;
	
	@Exclude()
  @Column({ nullable: false })
  password?: string;

  @JoinColumn()
  @OneToMany(type => Vehicle, vehicle => vehicle.user)
  vehicles: Vehicle[]
}
