import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseFilters,
  Req,
	UseInterceptors,
	ClassSerializerInterceptor,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { TypeORMFilter } from 'services/api/filter/typeorm.filter';
import { Roles } from 'services/api/decorator/Roles';
import { Role } from './enums/Role';
import { Public } from 'services/api/decorator/Public';

//POST https://somewebsitename.com/user/
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @UseFilters(new TypeORMFilter())
  @Public()
	@UseInterceptors(ClassSerializerInterceptor)
  async create(@Body() createUserDto: CreateUserDto) {
		const created = await this.userService.create(createUserDto);
    return created;
  }

	@Post("indexAll")
	@Public()
	indexAll(){
		return this.userService.indexAll();
	}

  @Get()
  @Public()
  findAll() {
    return this.userService.findAll();
  }

  @Get(':email')
  findOne(@Param('email') email: string) {
    return this.userService.findOneByEmail(email, false);
  }

  @Get('search/:text')
  search(@Param('text') text: string) {
    return this.userService.searchUsers(text);
  }

  @Patch(':id')
  @UseFilters(new TypeORMFilter())
  @Roles(Role.Owner, Role.VIPGodDeveloper)
  updateOther(@Param('id') id, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(id, updateUserDto);
  }

  @Patch()
  @UseFilters(new TypeORMFilter())
  update(@Req() req, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(req.user.id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.remove(id);
  }
}
