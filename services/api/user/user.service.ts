import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import {In, Repository} from 'typeorm';
import { User } from './entities/user.entity';
import UsersSearchService from "../search/users-search.service";

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private usersSearchService: UsersSearchService,
  ) {}

  async saveEntity(user: User) {
    await this.usersRepository.save(user);
  }

	async indexAll(){
		const users = await this.usersRepository.find();
		users.forEach(user => this.usersSearchService.indexUser(user));
		return "Operation send";
	}

  convertUserForPublic(user: User): User {
    const { password, ...safeData } = user;
    return safeData;
  }

  async isEmailAvailable(email: string): Promise<boolean> {
    const user = await this.usersRepository.findOneBy({ email });
    return !user;
  }

  async create(createUserDto: CreateUserDto): Promise<User | undefined> {
    const emailAvailable = await this.isEmailAvailable(createUserDto.email);

    if (!emailAvailable)
      throw new BadRequestException('This email is already taken');

    const user = await this.usersRepository.create(createUserDto);
    await this.usersRepository.save(user);
    await this.usersSearchService.indexUser(user);
    return user;
  }

  async findAll(): Promise<User[]> {
    const users = await this.usersRepository.find();
    return users.map(user => this.convertUserForPublic(user));
  }

  async findOneById(id: string, withPrivateFields: boolean = true, relations: string[] = []): Promise<User | undefined> {
    const user = await this.usersRepository.findOne({ where: { id }, relations });

    if (!user) throw new NotFoundException('User with given id was not found');

    if (!withPrivateFields) return this.convertUserForPublic(user);
    return user;
  }

  async findOneByEmail(email: string, withPrivateFields: boolean = true): Promise<User | undefined> {
    const user = await this.usersRepository.findOneBy({ email });

    if (!user) throw new NotFoundException('User with given email was not found');

    if(!withPrivateFields) return this.convertUserForPublic(user);
    return user;
  }

  async searchUsers(text: string) {
    const results = await this.usersSearchService.searchUsers(text);
    const ids = results.map(result => result.id);
    if (!ids.length) {
      return [];
    }
    return this.usersRepository.find({ where: { id: In(ids) } });
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<User | undefined> {
    if (updateUserDto.email) {
			const emailAvailable = await this.isEmailAvailable(updateUserDto.email);

    	if (!emailAvailable)
     		throw new BadRequestException('This email is already taken');
		}

    await this.usersRepository.update(id, updateUserDto);
    const user = await this.findOneById(id, false);
    await this.usersSearchService.updateUser(user);
    return user;
  }

  async remove(id: string): Promise<User | undefined> {
    const user = await this.findOneById(id);
    const deletedUser = await this.usersRepository.remove(user);
    await this.usersSearchService.removeUser(user.id);
    return deletedUser;
  }
}
