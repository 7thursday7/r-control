import { PartialType } from '@nestjs/mapped-types';
import { CreatePublicFileDto } from './create-public-file.dto';

export class UpdatePublicFileDto extends PartialType(CreatePublicFileDto) {}
