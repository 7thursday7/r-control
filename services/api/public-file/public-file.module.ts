import { Module } from '@nestjs/common';
import { PublicFileService } from './public-file.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PublicFile } from './entities/public-file.entity';

@Module({
  imports: [ConfigModule.forRoot(), TypeOrmModule.forFeature([PublicFile])],
  providers: [PublicFileService],
  exports: [PublicFileService]
})
export class PublicFileModule {}
