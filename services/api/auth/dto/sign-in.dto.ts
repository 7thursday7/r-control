import { IsDefined } from 'class-validator';

export class signInDto {
  @IsDefined()
  email: string;

  @IsDefined()
  password: string;
}