import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskController } from './task.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Task } from './entities/task.entity';
import { ActionModule } from 'services/api/action/action.module';

@Module({
	imports: [TypeOrmModule.forFeature([Task]), ActionModule],
  controllers: [TaskController],
  providers: [TaskService]
})
export class TaskModule {}
