import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './entities/task.entity';
import { Repository } from 'typeorm';
import { ActionService } from 'services/api/action/action.service';

@Injectable()
export class TaskService {
  constructor(
		@InjectRepository(Task)
		private taskRepository: Repository<Task>,
		private actionService: ActionService,
	) {}

	async create(CreateTaskDto: CreateTaskDto){
		const task = await this.taskRepository.create(CreateTaskDto);
		const action = await this.actionService.findOne(CreateTaskDto.action_id);
		task.action = action;

		this.taskRepository.save(task);
		return task;
	}

  async findAll() {
    return this.taskRepository.find();
  }

  async findOne(id: string) {
    const task = this.taskRepository.findOneBy({ id });

		if(!task)
		throw new NotFoundException('Task with given id was not found');

		return task;
  }

	async findActionTasks(id: string) {
		const action = await this.actionService.findOne(id, ['tasks']);
		return action.tasks;
	}

  async update(id: string, updateTaskDto: UpdateTaskDto) {
    this.taskRepository.update(id, updateTaskDto);
		const task = await this.findOne(id);
		return task;
  }

  async remove(id: string) {
		const task = await this.findOne(id);
		const deletedTask = await this.taskRepository.remove(task);
		return deletedTask;
	}
}

