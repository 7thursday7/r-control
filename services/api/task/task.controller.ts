import { Controller, Get, Post, Body, Patch, Param, Delete, UseFilters } from '@nestjs/common';
import { TaskService } from './task.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { TypeORMFilter } from 'services/api/filter/typeorm.filter';

@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Post()
	@UseFilters(new TypeORMFilter())
  create(@Body() createTaskDto: CreateTaskDto) {
    return this.taskService.create(createTaskDto);
  }

  @Get()
  findAll() {
    return this.taskService.findAll();
  }

  @Get(':id')
	@UseFilters(new TypeORMFilter())
  findOne(@Param('id') id: string) {
    return this.taskService.findOne(id);
  }

  @Get('action/:id')
	@UseFilters(new TypeORMFilter())
  findActionsTasks(@Param('id') id: string) {
    return this.taskService.findActionTasks(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTaskDto: UpdateTaskDto) {
    return this.taskService.update(id, updateTaskDto);
  }

  @Delete(':id')
	@UseFilters(new TypeORMFilter())
  remove(@Param('id') id: string) {
    return this.taskService.remove(id);
  }
}
