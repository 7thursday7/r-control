import { IsDefined, MaxLength, MinLength } from "class-validator";


export class CreateTaskDto {
	@MinLength(5)
	@MaxLength(60)
	@IsDefined()
	description: string;

	@IsDefined()
	action_id: string;
}
