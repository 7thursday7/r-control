import { Action } from '../../action/entities/action.entity';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Task {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@Column({nullable: false})
	description: string;

	@JoinColumn()
  @ManyToOne(type => Action, action => action.tasks, { cascade: true })
  action: Action;
}
