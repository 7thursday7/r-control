import { DataSource } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv';
import { User } from './services/api/user/entities/user.entity';
import { Vehicle } from './services/api/vehicle/entities/vehicle.entity';
import { Task } from './services/api/task/entities/task.entity';
import { Action } from './services/api/action/entities/action.entity';
import { PublicFile } from './services/api/public-file/entities/public-file.entity';
 
config();
 
const configService = new ConfigService();
 
export default new DataSource({
  type: 'postgres',
  host: configService.get('POSTGRES_HOST'),
  port: configService.get('POSTGRES_PORT'),
  username: configService.get('POSTGRES_USER'),
  password: configService.get('POSTGRES_PASSWORD'),
  database: configService.get('POSTGRES_DB'),
  entities: [User, Vehicle, Task, Action, PublicFile],
  migrations: ['src/migrations/*{.ts,.js}'],
  migrationsTableName: 'migrations',
});